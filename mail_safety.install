<?php

/**
 * @file
 * Install, update, and uninstall functions for the Mail Safety module.
 */

use Drupal\Core\Database\Database;

/**
 * Implements hook_schema().
 *
 * Define the schema's used for this module.
 */
function mail_safety_schema() {
  $schema['mail_safety_dashboard'] = [
    'description' => 'Stores all mails sent by the system in this table.',
    'fields' => [
      'mail_id' => [
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'The mail id.',
      ],
      'sent' => [
        'type' => 'int',
        'default' => 0,
        'description' => 'The order in which this display is loaded.',
      ],
      'mail' => [
        'type' => 'blob',
        'size' => 'big',
        'description' => 'A serialized mail object stored',
        'serialize' => TRUE,
        'serialized default' => 'a:0:{}',
      ],
    ],
    'primary key' => ['mail_id' => 'mail_id'],
  ];
  return $schema;
}

/**
 * Updates the mail column to use a blob instead of text.
 */
function mail_safety_update_8200() {
  $new_field = [
    'type' => 'blob',
    'size' => 'big',
    'description' => 'A serialized mail object stored',
    'serialize' => TRUE,
    'serialized default' => 'a:0:{}',
  ];

  Database::getConnection()->schema()->changeField('mail_safety_dashboard', 'mail', 'mail', $new_field);
}
