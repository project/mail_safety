<?php

namespace Drupal\mail_safety\ParamConverter;

use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\mail_safety\Controller\MailSafetyController;
use Symfony\Component\Routing\Route;

/**
 * Parameter converter for upcasting Mail Safety entity IDs to full objects.
 */
class MailSafetyParamConverter implements ParamConverterInterface {

  /**
   * {@inheritdoc}
   */
  public function convert($mail_id, $definition, $name, array $defaults) {
    return MailSafetyController::load($mail_id);
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    return (!empty($definition['type']) && $definition['type'] === 'mail_safety');
  }

}
