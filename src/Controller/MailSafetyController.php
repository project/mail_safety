<?php

namespace Drupal\mail_safety\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Theme\ThemeInitializationInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides the Mail Safety dashboard routes.
 */
class MailSafetyController implements ContainerInjectionInterface {

  /**
   * The config factor.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The Theme manager.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * Theme initialization.
   *
   * @var \Drupal\Core\Theme\ThemeInitializationInterface
   */
  protected $themeInitialization;

  public function __construct(ConfigFactoryInterface $configFactory, MailManagerInterface $mailManager, ThemeManagerInterface $themeManager, ThemeInitializationInterface $themeInitialization) {
    $this->configFactory = $configFactory;
    $this->mailManager = $mailManager;
    $this->themeManager = $themeManager;
    $this->themeInitialization = $themeInitialization;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.mail'),
      $container->get('theme.manager'),
      $container->get('theme.initialization'),
    );
  }

  /**
   * Let's the user view the e-mail caught by Mail Safety.
   *
   * @return array
   *   A render array of the e-mail content.
   */
  public function view($mail_safety) {
    $system = $this->getMailSystem($mail_safety['mail']);
    $mail_safety['mail'] = $system->format($mail_safety['mail']);

    return [
      '#theme' => 'mail_safety_mail',
      '#mail' => $mail_safety['mail'],
      '#mail_body_url' => Url::fromRoute('mail_safety.view_body', ['mail_safety' => $mail_safety['mail_id']]),
    ];
  }

  /**
   * Render the mail body without Drupal theme, for use in e.g. iframe.
   */
  public function viewBody($mail_safety) {
    // Switch the theme to the configured mail theme.
    $mail_theme = $this->getMailTheme();
    $current_active_theme = $this->themeManager->getActiveTheme();
    if ($mail_theme && $mail_theme !== $current_active_theme->getName()) {
      $this->themeManager->setActiveTheme($this->themeInitialization->initTheme($mail_theme));
    }

    $system = $this->getMailSystem($mail_safety['mail']);
    $message = $system->format($mail_safety['mail']);

    return new Response($message['body']);
  }

  /**
   * Let's the user view the details of an e-mail caught by Mail Safety.
   *
   * @return array
   *   A render array of the e-mail details.
   */
  public function details($mail_safety) {
    // Don't attempt to serialize unserializable objects.
    array_walk_recursive(
      $mail_safety['mail']['params'],
      function (&$value) {
        if (is_object($value) && !($value instanceof \Serializable)) {
          $value = t('** Not serializable **')->__toString();
        }
      }
    );
    return [
      '#theme' => 'mail_safety_details',
      '#mail' => $mail_safety['mail'],
      '#details' => print_r($mail_safety['mail'], TRUE),
    ];
  }

  /**
   * Load one or more mails.
   *
   * @param optional $mail_id
   *   If mail_id is not given it will load all the mails.
   *
   * @return array|bool
   *   Returns an array of one ore more mails.
   */
  public static function load($mail_id = NULL) {
    $mails = [];

    $connection = \Drupal::database();
    $query = $connection->select('mail_safety_dashboard', 'msd');
    $query->fields('msd', ['mail_id', 'sent', 'mail']);

    // Add a condition for the mail id is given.
    if (!is_null($mail_id)) {
      $query->condition('mail_id', $mail_id);
    }

    $query->orderBy('sent', 'DESC');

    $result = $query->execute();

    while ($row = $result->fetchAssoc()) {
      $mails[$row['mail_id']] = [
        'mail' => unserialize($row['mail']),
        'sent' => $row['sent'],
        'mail_id' => $row['mail_id'],
      ];
    };

    // Let other modules respond before a mail is loaded.
    // E.g. attachments that were saved with the mail.
    foreach ($mails as $key => &$mail) {
      \Drupal::moduleHandler()->invokeAllWith('mail_safety_load', function (callable $hook, string $module) use (&$mail) {
        $mail['mail'] = call_user_func($hook, $mail['mail']);
      });
    }

    if (!is_null($mail_id) && !empty($mails[$mail_id])) {
      return $mails[$mail_id];
    }
    elseif (!empty($mails)) {
      return $mails;
    }

    return $mails;
  }

  /**
   * Delete a mail from the database.
   *
   * @param int $mail_id
   *   The mail id.
   */
  public static function delete($mail_id) {
    $connection = \Drupal::database();
    $connection->delete('mail_safety_dashboard')
      ->condition('mail_id', $mail_id)
      ->execute();
  }

  /**
   * Saves the mail to the dashboard.
   *
   * @param array $message
   *   The drupal message array.
   */
  public static function insert(array $message) {
    // Let other modules alter the message array before a mail is inserted.
    // E.g. save attachments that are sent with the mail.
    \Drupal::moduleHandler()->alter('mail_safety_pre_insert', $message);

    $mail = [
      'sent' => time(),
      'mail' => serialize($message),
    ];

    $connection = \Drupal::database();
    $connection->insert('mail_safety_dashboard')
      ->fields($mail)
      ->execute();
  }

  /**
   * Get the mail system of the given mail.
   *
   * @param array $mail
   *   The mail array.
   *
   * @return object
   *   The mail system object.
   */
  protected function getMailSystem(array $mail) {
    return $this->mailManager->getInstance(['module' => $mail['module'], 'key' => $mail['key']]);
  }

  /**
   * Retrieves the key of the theme used to render the emails.
   */
  protected function getMailTheme() {
    $theme = $this->configFactory->get('mailsystem.settings')->get('theme');
    switch ($theme) {
      case 'default':
        $theme = $this->configFactory->get('system.theme')->get('default');
        break;

      case 'current':
        $theme = $this->themeManager->getActiveTheme()->getName();
        break;
    }
    return $theme;
  }

}
