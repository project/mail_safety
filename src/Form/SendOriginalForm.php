<?php

namespace Drupal\mail_safety\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the send mail to original mail address form.
 */
class SendOriginalForm extends ConfirmFormBase {

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The mail safety array.
   *
   * @var array
   */
  protected $mailSafety = [];

  /**
   * Constructs a \Drupal\statistics\StatisticsSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Mail\MailManagerInterface $mailManager
   *   The mail manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MailManagerInterface $mailManager, ModuleHandlerInterface $module_handler) {
    $this->configFactory = $config_factory;
    $this->mailManager = $mailManager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.mail'),
      $container->get('module_handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mail_safety_send_original_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to send "@subject" to @to', [
      '@subject' => $this->mailSafety['mail']['subject'],
      '@to' => $this->mailSafety['mail']['to'],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('mail_safety.dashboard');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Send original mail');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $mail_safety = NULL) {
    $this->mailSafety = $mail_safety;

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Resend the mail and bypass mail_alter by using the drupal_mail_system.
    $mail_array = $this->mailSafety['mail'];
    $mail_array['send'] = TRUE;

    // Let other modules respond before a mail is sent.
    // E.g. add attachments that were saved in the mail.
    $this->moduleHandler->invokeAllWith('mail_safety_pre_send', function (callable $hook, string $module) use (&$mail_array) {
      $mail_array = call_user_func($hook, $mail_array);
    });

    // Get the mail manager and the mail system because we already
    // got the e-mail during the mail function and want to skip drupal
    // parsing the mail again.
    $system = $this->getMailSystem($mail_array);
    $mail_array = $system->format($mail_array);
    $mail_array['result'] = $system->mail($mail_array);

    if ($mail_array['result']) {
      $this->messenger()->addStatus(t('Succesfully sent the message to @to', ['@to' => $mail_array['to']]));
    }
    else {
      $this->messenger()->addError(t('Failed to send the message to @to', ['@to' => $mail_array['to']]));
    }
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

  /**
   * Get the mail system of the given mail.
   *
   * @param array $mail
   *   The mail array.
   *
   * @return object
   *   The mail system object.
   */
  protected function getMailSystem(array $mail) {
    return $this->mailManager->getInstance(['module' => $mail['module'], 'key' => $mail['key']]);
  }

}
